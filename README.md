Bootstrap3 minimal wordpress skin
=================================

Contains no comments, no sidebar, no widgets.

Its navigation bar has a hack: it contains two menus, the first of which
is "Blog", having the categories as submenus, and the second of which is
"About", which contains the primary menu as a submenu. I couldn't find a
way in Wordpress of specifying all the categories as a submenu of a
menu (otherwise I could have simply used the primary menu in the
navbar).

© 2015 Antonis Christofides

© 2010 The guys who made theme twenty ten (some of whose code has been
copied).

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see http://www.gnu.org/licenses/.
